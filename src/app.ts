require('dotenv').config();
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as HttpStatus from 'http-status-codes';
import * as express from 'express';
import * as Knex from 'knex';
import { MySqlConnectionConfig } from 'knex';

const signale = require('signale');
const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const cors = require('cors');

import { Router, Request, Response, NextFunction } from 'express';
import { Jwt } from './models/jwt';
const jwt = new Jwt();
// Import routings
import indexRoute from './routes/index';
import User_m from './routes/users';
import loginRoute from './routes/login';
import icoopRouter from './routes/icoop';
import { decode } from 'jsonwebtoken';

const app: express.Application = express();

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100 // limit each IP to 100 requests per windowMs
});
var connection:MySqlConnectionConfig={
  host:process.env.DB_HOST,
  port:3306,
  database:process.env.DB_NAME,
  user:process.env.DB_USER,
  password:process.env.DB_PASSWORD

}
var connectionicoop:MySqlConnectionConfig={
  host:process.env.DBICOOP_HOST,
  port:3306,
  database:process.env.DBICOOP_NAME,
  user:process.env.DBICOOP_USER,
  password:process.env.DBICOOP_PASSWORD

}

var db:Knex=Knex({
  client:'mysql',
  connection:connection,
  pool:{
    min:0,
    max:100,
    afterCreate:(conn,done)=>{
conn.query('SET NAMES utf8',(err)=>{
done(err,conn);
})
    }
  }
});
var dbicoop:Knex=Knex({
  client:'mysql',
  connection:connectionicoop,
  pool:{
    min:0,
    max:100,
    afterCreate:(conn,done)=>{
conn.query('SET NAMES utf8',(err)=>{
done(err,conn);
})
    }
  }
});

var checkAuth= (req :Request,res,next)=>{
var token ='';
if(req.headers.authorization){
var _auth =req.headers.authorization.split(' ');

if(_auth[0]==='Bearer'){
token =_auth[1];
}
}else if (req.query.token){
token= req.query.token;
}else if (req.body.token){
  token= req.body.token;
  }
jwt.verify(token).then(decode=>{
  req.decoded=decode;
  next();

}).catch(()=>{
res.send({ok:false,error:HttpStatus.UNAUTHORIZED,
code:HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)});
});

}
app.use((req: Request, res: Response, next: NextFunction) => {
  req.logger = signale;
  next();
});

app.use(limiter);
app.use(helmet({ hidePoweredBy: { setTo: 'PHP 4.2.0' } }));
app.use(cors());

// app.use(logger('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.get('/favicon.ico', (req: Request, res: Response) => res.status(204));
app.use((req,res,next)=>{
req.db =db;
req.dbicoop =dbicoop;

next();
});
app.use('/', indexRoute);
app.use('/user',checkAuth ,User_m);
app.use('/login', loginRoute);
app.use('/icoop', icoopRouter);

//error handlers

if (process.env.NODE_ENV === 'development') {
  app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    req.logger.error(err.stack);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      error: {
        ok: false,
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
      }
    });
  });
}

app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(HttpStatus.NOT_FOUND).json({
    error: {
      ok: false,
      code: HttpStatus.NOT_FOUND,
      error: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
    }
  });
});

export default app;
