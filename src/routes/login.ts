/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as crypto from 'crypto';
import { LoginModel } from '../models/login';
import { Jwt } from '../models/jwt';
const jwt =new Jwt();
const login = new LoginModel();
const router: Router = Router();
router.get('/', (req: Request, res: Response) => {
    // req.logger.info('test logger');
    res.send({ ok: true, message: 'Welcome to RESTful api login!', code: HttpStatus.OK });
  });
router.post('/', async(req: Request, res: Response) => {
 const username:string = req.body.username;
 const password:string = req.body.password;

 try {
    const encpass = crypto.createHash('md5').update(password).digest('hex');
    const rs :any = await  login.login(req.db,username,encpass); 
    
    if(rs.length){
        var payload={
                id:rs[0],
       fullname:rs[0].fullname
        }
   var token =jwt.sign(payload);
   res.send({ok:true,token:token});
    }else{
        res.send({ ok: false, error: 'ไม่มีผู้ใช้งานนี้'});  
    }
 } catch (error) {
  res.send({ ok: false, error: 'ไม่มีผู้ใช้งานนี้'});   
 }
  
});


export default router;