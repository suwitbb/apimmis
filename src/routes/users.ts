/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import { UserModel } from '../models/user';
import * as crypto from 'crypto';
const userModel= new UserModel();
const router: Router = Router();

router.get('/', (req: Request, res: Response) => {
  // req.logger.info('test logger');
  res.send({ ok: true, message: 'Welcome to RESTful user', code: HttpStatus.OK });
});
router.get('/detial', async(req: Request, res: Response) => {
  try {
    var rs:any = await userModel.getlist(req.db);
    res.send({ok:true,rows:rs});
  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500   });
  }
});
router.post('/detial',async (req: Request, res: Response) => {
  const name: String = req.body.name; 
  const lname: String = req.body.lname;
  const pass: String = req.body.pass; 
  try {
    var data :any ={
username:name,
password:pass,
fullname:`${name} ${lname}`
    }
    var rs:any = await userModel.save(req.db,data);

    
    res.send({ok:true});
  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500   });
  }
  
});
router.put('/detial', async(req: Request, res: Response) => {
  const id :any =req.body.id;
  const name: String = req.body.name; 
  const lname: String = req.body.lname;
 const password: any = req.body.pass; 
  try {
    var ensPass= crypto.createHash('md5').update(password).digest('hex');
    var data :any ={
username:name,
password:ensPass,
fullname:`${name} ${lname}`
    }
    var rs:any = await userModel.update(req.db,id,data);

    
    res.send({ok:true});
  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500   });
  }



  res.send({ ok: true, message: 'Welcome to RESTful put', code: HttpStatus.OK });
});
router.delete('/detial', async(req: Request, res: Response) => {
const id :string= req.body.id;
try {
  if(id){
 await userModel.delete(req.db,id);
 res.send({ ok: true});
  }else{
    res.send({ ok: false, error:'ไม่พบID'});
  }
 
} catch (error) {
  res.send({ ok: false, error: 'error delete'});
}
  
});
router.get('/user', (req: Request, res: Response) => {
  const a: number = req.query.a; 
  const b: number = req.query.b; 
  console.log(a+'==='+b);
  req.logger.info(a+'==='+b);
  req.logger.error(a+'==='+b);
  res.send({ ok: true, message: 'Welcome to api user!', code: HttpStatus.OK });
});
router.get('/users/:sex/:age', (req: Request, res: Response) => {
  const a: String = req.params.sex; 
  const b: number = req.params.age; 
  console.log(a+'==='+b);
  req.logger.info(a+'==='+b);
  req.logger.error(a+'==='+b);
  res.send({ ok: true, message: 'Welcome to api user!', code: HttpStatus.OK });
});

export default router;