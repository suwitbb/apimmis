/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
const router: Router = Router();

router.get('/', (req: Request, res: Response) => {
  // req.logger.info('test logger');
  res.send({ ok: true, message: 'Welcome to RESTful api icoop!', code: HttpStatus.OK });
});

export default router;