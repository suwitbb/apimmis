import * as Knex from 'knex';
export class UserModel{
    getlist(db:Knex){
  return  db('usernames').select('id','fullname','username'); 
    }
 save(db:Knex,data:any){
 return db('usernames').insert(data);
 }
 update(db:Knex,id:any,data:any){
  return db('usernames').update(data).where('id',id);
  }
  delete(db:Knex,id:any){
    return db('usernames').where('id',id).del();
    }
}